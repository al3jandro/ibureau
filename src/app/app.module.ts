import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { BinaryService } from './shared/services/binary.service';
import { BinaryComponent } from './layout/binary/binary.component';
import { ArrayComponent } from './layout/array/array.component';
import { RouteModule } from './route/route.module';
import { HomeComponent } from './layout/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    BinaryComponent,
    ArrayComponent,
    HomeComponent
  ],
  imports: [
    RouteModule,
    BrowserModule,
    FormsModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [BinaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
