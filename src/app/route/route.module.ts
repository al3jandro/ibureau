import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BinaryComponent } from '../layout/binary/binary.component';
import { HomeComponent } from '../layout/home/home.component';
import { ArrayComponent } from '../layout/array/array.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'binary', component: BinaryComponent },
  { path: 'array', component: ArrayComponent },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class RouteModule { }
