import { Injectable } from '@angular/core';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Injectable({
  providedIn: 'root'
})

export class BinaryService {

  exit: string;
  entry: number;
  count: number;
  max: any;
  constructor() { }

  getBinaryDigit(N) {
    let digit = 2;
    while ( N >= digit ) { digit *= 2; }
    return digit / 2;
  }

  IntBinary(value) {
    let result = '';
    for ( let digit = this.getBinaryDigit(value); digit >= 1; digit /= 2 ) {
        if ( value >= digit ) {
            value -= digit;
            result += '1';
        } else { result += '0'; }
    }
    return result;
  }

  countNumber(str, letter) {
    this.count = 0;
    for (let position = 0; position < str.length; position++) {
      if (str.charAt(position) === letter) {
        this.count += 1;
      }
    }
    return this.count;
  }

  zeroSeq(value) {
    let result = 0;
    let current = 0;
    const str = value.toString(2);
    let startIndexFromEnd = str.length - 1;
    for (startIndexFromEnd; startIndexFromEnd >= 0; startIndexFromEnd--) {
        if (str.charAt(startIndexFromEnd) !== '0') {
            break;
        }
    }
    for (let i = startIndexFromEnd - 1; i >= 0; i--) {
        if (str.charAt(i) === '0') {
            current = current + 1;
        } else {
            if (current > result) {
                result = current;
            }
            current = 0;
        }
    }
    return result;
  }

  convertStringArray(value) {
    const result = value.split('');
    return result;
  }

  arrayMove(value, n) {
    const arr = value.length;
    console.log('N ', n);
    console.log('Array ', arr);
    const result = [];
    if (n < 0 || n > 100 || arr === 0) {
        return result;
    }
    if (arr === 1) {
        return value;
    }
    for (let i = 0; i < arr; i++) {
        result[(i + n) % arr] = value[i];
    }
    console.log('Result ', result);
    return result;
  }
}
