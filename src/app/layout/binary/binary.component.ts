import { Component, OnInit } from '@angular/core';
import { BinaryService } from 'src/app/shared/services/binary.service';

@Component({
  selector: 'app-binary',
  templateUrl: './binary.component.html',
  styleUrls: ['./binary.component.scss']
})
export class BinaryComponent implements OnInit {

  binario: string;
  entero: number;
  zero: number;
  um: number;
  seq: number;

  constructor(private bin: BinaryService) { }

  ngOnInit() { }

  submit() {
    this.entero = this.entero;
    this.binario = this.bin.IntBinary(this.entero);
    this.zero = this.bin.countNumber(this.binario, '0');
    this.um  = this.bin.countNumber(this.binario, '1');
    this.seq = this.bin.zeroSeq(this.binario);
  }

}
