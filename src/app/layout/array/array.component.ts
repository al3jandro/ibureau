import { Component, OnInit } from '@angular/core';
import { BinaryService } from 'src/app/shared/services/binary.service';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.scss']
})
export class ArrayComponent implements OnInit {

  arr: string;
  result: any = [];
  repeat: number;
  matriz: string;
  flag = false;
  constructor(private bin: BinaryService) { }

  ngOnInit() {
    console.log(this.bin.arrayMove('12345678', 3));
  }

  submit() {
    this.flag = true;
    this.repeat = this.repeat;
    this.result = this.bin.arrayMove(this.arr.toString(), this.repeat);
    this.matriz = this.bin.convertStringArray(this.arr.toString());
  }
}
